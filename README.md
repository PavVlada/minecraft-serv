# UPD
1. В первом плэе устанавливается openjdk-17. Это действие происходит от имени рута, потому был создан отдельный плэй, который выполняет действия от имени пользователя:   
1) Настройку сервера с forge  
2) Устанавка модов 
4) Запуск сервера  

2. Скачивание модов в цикле в недавно созданную директорию  
```
- name: Download comfort mod
  get_url:
    url: "{{ item }}"
    dest: "{{ minecraft_home }}/mods"
  loop: "{{ mod_url }}"

```
3. Параметризация server.properties.j2:
```
allow-flight={{ allow_flight }}
allow-nether={{ allow_nether }}
broadcast-console-to-ops={{ broadcast_console_to_ops }}
broadcast-rcon-to-ops={{ broadcast_rcon_to_ops }}
difficulty={{ difficulty }}
...
```

# Screeen  
Скрин, как подтверждение того, что сервер был поднят, и мод Comforts был загружен  
![sample](screen/screen-mod.png)

# Playbooks

Плэйбук содержит в себе 2 плэя  
В первом плэе устанавливается openjdk-17. Это действие происходит от имени рута, потому был создан отдельный плэй, который выполняет действия от имени пользователя: 
1) Настройку сервера с forge  
2) Устанавка модов 
4) Запуск сервера 
```
- name: Set up minecraft server
  hosts: all
  gather_facts: no
  become: yes
  roles:
    - name: openjdk-17

- name: Set up minecraft server
  hosts: all
  gather_facts: no
  become_user: "{{ ansible_ssh_user }}"
  roles:  
    - name: set-up-forge-serv
    - name: mods
    - name: start-minecraft

```
# Roles
## openjdk-17

Обновление пакетов  
```
- name: Upgrade all packages
  yum:
    name: '*'
    state: latest

```
Установка unzip
```
- name: Install unzip
  yum:
    name: unzip

```
Скачивание openjdk-17 с ссылки, указанной в defaults, и разархивирование его в /opt/
```
- name: Unarchieve openjdk-17
  unarchive:
    src: "{{ openjdk17_url }}"
    dest: /opt/
    remote_src: yes

```
Настройка окружения java для пользователя, через которого идет подключение по ssh  
```
- name: Set java environment
  blockinfile:
    create: yes
    path: /home/{{ ansible_ssh_user }}/.bashrc
    block: |
      export JAVA_HOME=/opt/{{ openjdk17_home }}
      export PATH=$PATH:$JAVA_HOME/bin
```
## set-up-forge-serv
Создание директории, где будет храниться minecraft
```
- name: Create minecraft home directory
  file: 
    path: "{{ minecraft_home }}"
    state: directory

```
Скачивание jar сервера с forge
```
- name: Download forge's jar file
  get_url:
    url: "{{ forge_url }}"
    dest: "{{ minecraft_home }}"

```
Принятие условия лицензионного соглашения посредством создания файла и вписания в ней строки
```
- name: Agree to eula
  lineinfile:
    create: yes
    path: "{{ minecraft_home }}/eula.txt"
    line: eula=true

```
Установка сервера с forge
```
- name: Install forge
  shell:
    cmd: java -jar {{ forge_jar }} --installServer
    chdir: "{{ minecraft_home }}"

```
Внесение правок в настройки игры (в т.ч. смены режима игра на "creative")
```
- name: Changing server.properties
  template:
    src: server.properties.j2
    dest: "{{ minecraft_home }}/server.properties"

```
## mods
Создание папки, куда будет помещен мод
```
- name: Create minecraft mods  directory
  file: 
    path: "{{ minecraft_home }}/mods"
    state: directory
```
Скачивание модов в цикле в недавно созданную директорию
```
- name: Download comfort mod
  get_url:
    url: "{{ item }}"
    dest: "{{ minecraft_home }}/mods"
  loop: "{{ mod_url }}"

```
## start-minecraft
Запуск сервера
```
- name: Start minecraft server
  shell:
    cmd: ./run.sh --nogui
    chdir: "{{ minecraft_home }}"

```
# start ansible
```
ansible-playbook playbooks/minecraft_pb.yaml -i hosts -v
```
